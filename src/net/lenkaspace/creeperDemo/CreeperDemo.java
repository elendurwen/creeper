package net.lenkaspace.creeperDemo;

import java.awt.Dimension;

import net.lenkaspace.creeper.CRController;
import net.lenkaspace.creeper.model.CRBaseSituatedModel;
import net.lenkaspace.creeper.model.CRBaseSituatedModel.SHAPE;
import net.lenkaspace.creeper.model.CRParameters;
import net.lenkaspace.creeper.report.CRTimeSeriesReport;
import net.lenkaspace.creeper.view.CRRenderer;
import net.lenkaspace.creeper.vo.CRVector3d;
import net.lenkaspace.creeperDemo.images.ImageProvider;
import net.lenkaspace.creeperDemo.model.Agent;

public class CreeperDemo {
	
	private CRController crController;
	
	public CreeperDemo() {
		
		//---- create CRController
		crController = new CRController("Creeper demo");
		
		//---- set basic parameters
		CRParameters parameters = CRParameters.getSingleton();
		parameters.trialDuration = 1000;
		parameters.numOfRuns = 1;
		parameters.reportFolderName = "MyReports";
		
		//---- create image provider
		ImageProvider imageProvider = new ImageProvider();
		crController.setImageProvider(imageProvider);
		
		//---- optionally setup new world and renderer size...
		/*World world = new World(crController);
		crController.setWorld(world);
		crController.getRenderer().setPreferredSize(new Dimension(660,660));*/
		
		//-- ... or optionally create objects and pass them to the existing world
		CRBaseSituatedModel obstacle = new CRBaseSituatedModel(0, new CRVector3d(350,325,0), new CRVector3d(100,100,0), 180, SHAPE.CIRCLE, CRRenderer.CR_GREEN_DOT);
		Agent agent1 = new Agent(0, new CRVector3d(200,200,0), 0);
		Agent agent2 = new Agent(1, new CRVector3d(300,200,0), 45);
		Agent agent3 = new Agent(2, new CRVector3d(400,200,0), 180);
		
		crController.getWorld().addSituatedModel(obstacle); //using the default CRWorld of CRController
		crController.getWorld().addDynamicModel(agent1);
		crController.getWorld().addDynamicModel(agent2);
		crController.getWorld().addDynamicModel(agent3);
		
		//---- optionally setup reports
		CRTimeSeriesReport report1 = new CRTimeSeriesReport("Agent acceleration", new String[] {"Agent 1","Agent 2","Agent 3"}, new Dimension(1000,500));
		crController.getReportController().addReport("Agent acceleration", report1);
		
		//-- tell crController to rebuild the control panel because we added reports...
		crController.createDefaultControlPanel();
		
		//-- ... or optionally create new control panel that extends from CRControlPanel and set it
		/*ControlPanel controlPanel = new ControlPanel(485, 600, crController);
		crController.setControlPanel(controlPanel);*/
		
		//---- start CRController's main thread programatically if needed
		//crController.startSimulation("myReport", 120, 1);
		
      
	}
	
	/**
	 * Main function
	 */
	public static void main(String[] args) {
		new CreeperDemo();
	}
        
            
    

}
