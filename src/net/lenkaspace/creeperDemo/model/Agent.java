package net.lenkaspace.creeperDemo.model;

import net.lenkaspace.creeper.helpers.CRMaths;
import net.lenkaspace.creeper.model.CRBaseDynamicModel;
import net.lenkaspace.creeper.report.CRTimeSeriesReport;
import net.lenkaspace.creeper.vo.CRVector3d;

/**
 * An agent accelerates randomly and notes that acceleration into a report.
 * 
 * @author 		Lenka contact@lenkaspace.net
 * @version     2.0   
 *
 */
public class Agent extends CRBaseDynamicModel {

	public Agent(int id_, CRVector3d position_, double rotation_) {
		super(id_, position_, new CRVector3d(60,60,0), rotation_, SHAPE.CIRCLE, "agent.png");
	}
	
	public void update() {
		//-- randomly accelerate
		this.setThrustForce(CRMaths.getRandomGaussian(this.getThrustForce(), 0.3));
		
		//-- save acceleration to report, based on id
		((CRTimeSeriesReport)world.getController().getReportController().getReport("Agent acceleration")).addValue(this.getThrustForce(), id);
		//-- move
		super.update();
	}
	
}
