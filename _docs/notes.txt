NEEDS DOCUMENTING

- class diagram
- base class example + what is trial, what is run 
- creating lists of models (should be added to existing list, lowest in hierarchy)
- events
- how to update help

- how the Cpanel is added + how things are added to subclass of Cpanel (parents must be existing JPanels that need to be resized, because Java doesn't allow to call contructor after statements - use setPreferredSize)

- how a singleton object can be used to store settings from UI and then pulled from anywhere to access these settings

- how to change initial text field values of cpanel - from a subclass and from Applet

- sublcass CRReportController to save from other reports to csv reports when simulation events happen. Then set it as reference of CRController. Then Control panel needs to be recreated


NEEDS DOING
- if added to startSimulationPanel, start from the bottom so that new things added are above the Start Sim button
- new simulation: check if update should finish first


